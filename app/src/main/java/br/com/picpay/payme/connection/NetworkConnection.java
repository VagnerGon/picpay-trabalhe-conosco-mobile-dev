package br.com.picpay.payme.connection;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.com.picpay.payme.contract.Card;
import br.com.picpay.payme.contract.Contact;
import br.com.picpay.payme.contract.ResponseStatus;

public class NetworkConnection implements IConnection {

    private static String protocol = "http://";
    private static URL usersUrl;
    private static URL paymentUrl;
    private static RequestQueue requestQueue;
    private static ImageLoader imageLoader;

    public NetworkConnection(Context context) {
        try {
            if (requestQueue == null)
                requestQueue = Volley.newRequestQueue(context.getApplicationContext());
            if (imageLoader == null)
                imageLoader = new ImageLoader(requestQueue, new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap> cache = new LruCache<>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });

            //noinspection SpellCheckingInspection
            URL baseUrl = new URL(protocol + "careers.picpay.com/tests/mobdev/");
            usersUrl = new URL(baseUrl + "users");
            paymentUrl = new URL(baseUrl + "transaction");
        } catch (Exception ex) {
            Log.e(NetworkConnection.class.getName(), ex.getMessage(), ex);
        }
    }

    @Override
    public void getContactList(int take, int skip, final ContactListResponse contactListResponse) {
        try {
            requestQueue.add(new JsonArrayRequest(usersUrl.toString(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {
                        try {
                            List<Contact> contacts = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); ++i){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                final Contact contact = new Contact();
                                contact.setName(jsonObject.getString("name"));
                                contact.setProfilePicUrl(jsonObject.getString("img"));
                                imageLoader.get(contact.getProfilePicUrl(), new ImageLoader.ImageListener() {
                                    @Override
                                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                                        contact.setProfilePic(response.getBitmap());
                                        contactListResponse.notifyImageLoaded();
                                    }

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e(NetworkConnection.class.getName(), error.getMessage(), error);
                                    }
                                });
                                contacts.add(contact);
                            }
                            contactListResponse.getContactListResult(contacts);
                        } catch (Exception ex) {
                            contactListResponse.onError(ex);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        contactListResponse.onError(error);
                    }
                })
            );
        } catch (Exception ex) {
            contactListResponse.onError(ex);
            Log.e(NetworkConnection.class.getName(), ex.getMessage(), ex);
        }
    }

    @Override
    public void payToContact(Contact contact, Card card, float value, PayToContactResponse payToContactResponse) {
        try {
            payToContactResponse.getPaymentResult(ResponseStatus.SUCCESSFUL, "");
        } catch (Exception ex) {
            payToContactResponse.onError(ex);
            // TODO log error
        }
    }

    @Override
    public void clearCache() {
        NetworkConnection.imageLoader = null;
    }
}
