package br.com.picpay.payme.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.picpay.payme.R;
import br.com.picpay.payme.connection.IConnection;
import br.com.picpay.payme.connection.NetworkConnection;
import br.com.picpay.payme.contract.Contact;

public class ContactFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private ContactItemFragmentListener listener;
    private ContactViewAdapter contactViewAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    private void getContactList() {
        getContactList(this.getContext());
    }
    private void getContactList(Context context){
        new NetworkConnection(context).getContactList(10, 0, new IConnection.ContactListResponse() {
            @Override
            public void getContactListResult(List<Contact> contacts) {
                contactViewAdapter.setContactList(contacts);
                swipeRefreshLayout.setRefreshing(false);
            }
            @Override
            public void onError(Exception exception) {
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void notifyImageLoaded() {
                contactViewAdapter.notifyDataSetChanged();
            }
        });
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);

        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.contact_list);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefresh);
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        this.contactViewAdapter = new ContactViewAdapter(listener);
        recyclerView.setAdapter(this.contactViewAdapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ContactItemFragmentListener) {
            listener = (ContactItemFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement ContactItemFragmentListener");
        }
        getContactList(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        getContactList();
    }

    public interface ContactItemFragmentListener {
        void contactSelectedOnClick(Contact contactListItem, ContactViewAdapter.ContactItemViewHolder contactListItemView);
    }
}
