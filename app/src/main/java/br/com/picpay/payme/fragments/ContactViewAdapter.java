package br.com.picpay.payme.fragments;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.picpay.payme.R;
import br.com.picpay.payme.contract.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactViewAdapter extends RecyclerView.Adapter<ContactViewAdapter.ContactItemViewHolder> {

    private List<Contact> contacts;
    private final ContactFragment.ContactItemFragmentListener listener;

    public ContactViewAdapter(ContactFragment.ContactItemFragmentListener _listener) {
        this.listener = _listener;
        this.contacts = new ArrayList<>();
    }

    @NonNull
    @Override
    public ContactItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item, parent, false);
        return new ContactItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactItemViewHolder holder, int position) {
        if (position >= contacts.size())
            return;
        holder.textView.setText(contacts.get(position).getName());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int holderPosition = holder.getAdapterPosition();
                if (holderPosition >= contacts.size())
                    return;
                listener.contactSelectedOnClick(contacts.get(holderPosition), holder);
            }
        });
        Bitmap avatar = contacts.get(position).getProfilePic();
        if (avatar != null)
        holder.imageView.setImageBitmap(avatar);
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    public void setContactList(List<Contact> contacts) {
        this.contacts = contacts;
        this.notifyDataSetChanged();
    }

    public class ContactItemViewHolder extends RecyclerView.ViewHolder {
        final View view;
        final ImageView imageView;
        final TextView textView;

        ContactItemViewHolder(View _view) {
            super(_view);
            view = _view;
            imageView = (ImageView) view.findViewById(R.id.contact_avatar);
            textView = (TextView) view.findViewById(R.id.contact_name);
        }

        public View getView() {
            return view;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public TextView getTextView() {
            return textView;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + textView.getText() + "'";
        }
    }
}
