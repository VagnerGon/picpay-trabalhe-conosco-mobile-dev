package br.com.picpay.payme.contract;

public enum ResponseStatus {
    SUCCESSFUL,
    ERROR,
    WARNING
}
