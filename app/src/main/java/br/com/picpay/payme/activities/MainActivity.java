package br.com.picpay.payme.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import br.com.picpay.payme.R;
import br.com.picpay.payme.contract.Contact;
import br.com.picpay.payme.fragments.ContactFragment;
import br.com.picpay.payme.fragments.ContactViewAdapter;

public class MainActivity extends AppCompatActivity implements ContactFragment.ContactItemFragmentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void contactSelectedOnClick(Contact contactListItem, ContactViewAdapter.ContactItemViewHolder contactListItemView) {
        Intent intent = new Intent(this, PayToActivity.class);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, contactListItemView.getImageView(), getString(R.string.moving_forward));
        this.startActivity(intent, options.toBundle());
    }
}
