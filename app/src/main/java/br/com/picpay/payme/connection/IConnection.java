package br.com.picpay.payme.connection;

import java.util.List;

import br.com.picpay.payme.contract.Card;
import br.com.picpay.payme.contract.Contact;
import br.com.picpay.payme.contract.ResponseStatus;

public interface IConnection {

    void getContactList(int take, int skip, ContactListResponse contactListResponse);
    void payToContact(Contact contact, Card card, float value, PayToContactResponse payToContactResponse);
    void clearCache();

    interface ContactListResponse {
        void getContactListResult(List<Contact> contacts);
        void onError(Exception exception);
        void notifyImageLoaded();
    }

    interface PayToContactResponse {
        void getPaymentResult(ResponseStatus status, String message);
        void onError(Exception exception);
    }
}


